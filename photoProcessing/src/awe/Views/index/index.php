<h1 class="hcategory">Page of Category's Choice</h1>
<p>
    <?php if ($categories): ?>
<div id="filters">
    <div class="carousel-wrapper">
        <div data-jcarousel="true" class="carousel">
            <ul>
                <?php foreach ($categories as $oneCategory): ?>
                    <li class="hcategory">
                        <a href="/<?= SITE_ROOT_NAME ?>category/?id=<?= $oneCategory['id'] . '"> <img src="../../../../web/img/folder.png" title="' . $oneCategory['name'] ?> "></a>

                        <p> <?= $oneCategory['name'] ?></p>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <a data-jcarousel-control="true" data-target="-=3" href="#" class="carousel-control-prev">&lsaquo;</a>
        <a data-jcarousel-control="true" data-target="+=3" href="#" class="carousel-control-next">&rsaquo;</a>
    </div>
</div>
<?php else: ?>
    <h2 class="hcategory">Something wrong!</h2>
<?php
endif; ?>

</p>





