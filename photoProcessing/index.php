<?php
// включим отображение всех ошибок
error_reporting (E_ALL);


// подключаем конфиг
include_once ('app/config/config.php');

//session_destroy();
//session_regenerate_id();

// Соединяемся с БД
$dbObject = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
$dbObject->exec('SET CHARACTER SET utf8');

// подключаем ядро сайта
include (SITE_PATH . 'src'. DS . 'awe'. DS . 'core' . DS . 'core.php');


// Загружаем router
$router = new Router();
// задаем путь к папке с контроллерами.
$router->setPath ('src' . DS . 'awe' . DS . 'Controllers');

// запускаем маршрутизатор
$router->start();