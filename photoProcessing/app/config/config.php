<?php
// Задаем константы:
define ('DS', DIRECTORY_SEPARATOR); // разделитель для путей к файлам
$sitePath = realpath(dirname("/") . DS) . DS;
define ('SITE_PATH', $sitePath); // путь к корневой папке сайта
define ('SITE_ROOT_NAME', basename($sitePath));

// пути к папкам с ресурсами
$root_dir = 'photoProcessing';
define ('BASE_RESOURSE_PATH', explode($root_dir, __DIR__)[0] . $root_dir. DS .'resourses' .DS);

// для подключения к бд
define('DB_USER', 'mysql');
define('DB_PASS', 'mysql');
define('DB_HOST', 'localhost');
define('DB_NAME', 'photo_effect');


//////////////// work with sessions and cookies;

session_start();
setcookie('SID', session_id());

if (!isset( $_COOKIE['SID'])){
    header('Location: index.php');
}

if(!isset($_SESSION['imageIterator']) || !isset( $_SESSION['imgArray']) ){
    $_SESSION['imageIterator'] = null;
    $_SESSION['imgArray'][0] = 'tmp.jpg';
}

$tmpDir = __DIR__ . './../../temp/';
$newSessionDir = $tmpDir . $_COOKIE['SID'];

if (!file_exists($newSessionDir) ){
	mkdir($newSessionDir);
}
