<?php

Class Model_Category Extends Model_Base
{

    public $id;
    public $name;
    public $owner;
    public $description;

    public function fieldsTable()
    {
        return array(

            'id' => 'id',
            'name' => 'name',
            'owner' => 'owner',
            'description' => 'description',
        );
    }

}