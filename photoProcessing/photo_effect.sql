-- phpMyAdmin SQL Dump
-- version 4.0.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 16 2014 г., 09:27
-- Версия сервера: 5.5.38-log
-- Версия PHP: 5.5.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `photo_effect`
--

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `owner` int(11) unsigned DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `name`, `owner`, `description`) VALUES
(1, 'Filters', NULL, NULL),
(2, 'Effects', NULL, NULL),
(3, 'Frames', NULL, NULL),
(4, 'Textures', NULL, NULL),
(5, 'Vignettes', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `filter`
--

CREATE TABLE IF NOT EXISTS `filter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `settings` text NOT NULL,
  `preview` varchar(255) NOT NULL,
  `popularity` int(11) unsigned NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `category_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_CATEGORY` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=130 ;

--
-- Дамп данных таблицы `filter`
--

INSERT INTO `filter` (`id`, `name`, `settings`, `preview`, `popularity`, `description`, `category_id`) VALUES
(1, 'grayscale', '[{"grayscale":[]}]', 'grayscale', 0, NULL, 1),
(2, 'adaptiveSharpen', '[{"adaptiveSharpen":[1.5,3]}]', 'adaptiveSharpen', 0, NULL, 1),
(3, 'gotham', '[{"idf=4:[100,10,100]"},{"idf=14":[75]},{"idf=52":[]},{"idf=24":[]}]', 'gotham', 0, NULL, 2),
(4, 'modulate', '[{"modulate":[110.,120.,140.]}]', 'modulate', 0, NULL, 1),
(5, 'adaptiveBlur', '[{"adaptiveBlur":[5,3,"Imagick::CHANNEL_DEFAULT"]}]', 'adaptiveBlur', 0, NULL, 1),
(6, 'sharpen', '[{"sharpen":[2.,1.,"Imagick::CHANNEL_ALL"]}]', 'sharpen', 0, NULL, 1),
(7, 'adaptiveSharpen', '[{"adaptiveSharpen":[3.,1.,"Imagick::CHANNEL_ALL"]}]', 'adaptiveSharpen', 0, NULL, 1),
(8, 'unsharpMask', '[{"unsharpMaskSharpen":[3.,1.,1.,0.,"Imagick::CHANNEL_ALL"]}]', 'unsharpMask', 0, NULL, 1),
(9, 'contrast', '[{"contrast":[1]}]', 'contrast', 0, NULL, 1),
(10, 'contrastStretch', '[{"contrastStretch":[50,50,"Imagick::CHANNEL_ALL"]}]', 'contrastStretch', 0, NULL, 1),
(11, 'modulateRandom', '[{"modulateRandom":[]}]', 'modulateRandom', 0, NULL, 1),
(12, 'hue', '[{"hue":[120]}]', 'hue', 0, NULL, 1),
(13, 'saturation', '[{"saturation":[130]}]', 'saturation', 0, NULL, 1),
(14, 'brightness', '[{"brightness":[130]}]', 'brightness', 0, NULL, 1),
(15, 'invert', '[{"invert":[false,"Imagick::CHANNEL_ALL"]}]', 'invert', 0, NULL, 1),
(16, 'sepia', '[{"sepia":[80.]}]', 'sepia', 0, NULL, 1),
(17, 'recolor', '[{"recolor":[[1,0,0,0,0,1,0,1,0,]]}]', 'recolor', 0, NULL, 1),
(18, 'crop', '[{"crop":[433,600,100,100]}]', 'crop', 0, NULL, 1),
(19, 'cropThumbnail', '[{"cropThumbnail":[300,300]}]', 'cropThumbnail', 0, NULL, 1),
(20, 'resize', '[{"resize":[250,350,false,0.9,"Imagick::FILTER_LANCZOS"]}]', 'resize', 0, NULL, 1),
(21, 'vignette', '[{"vignette":[30.,30.,0,0]}]', 'vignette', 0, NULL, 1),
(22, 'horizontalMirroring', '[{"horizontalMirroring":[]}]', 'horizontalMirroring', 0, NULL, 1),
(23, 'verticalMirroring', '[{"verticalMirroring":[]}]', 'verticalMirroring', 0, NULL, 1),
(24, 'setFrame', '[{"setFrame":["#aabbcc",20,20,5,5]}]', 'setFrame', 0, NULL, 1),
(25, 'blur', '[{"blur":[5,3,"Imagick::CHANNEL_DEFAULT"]}]', 'blur', 0, NULL, 1),
(26, 'gaussianBlur', '[{"gaussianBlur":[5,3,"Imagick::CHANNEL_ALL"]}]', 'gaussianBlur', 0, NULL, 1),
(27, 'motionBlur', '[{"motionBlur":[15,13,140,"Imagick::CHANNEL_ALL"]}]', 'motionBlur', 0, NULL, 1),
(28, 'radialBlur', '[{"radialBlur":[5,"Imagick::CHANNEL_ALL"]}]', 'radialBlur', 0, NULL, 1),
(29, 'emboss', '[{"emboss":[0,1]}]', 'emboss', 0, NULL, 1),
(30, 'addNoise', '[{"addNoise":["Imagick::NOISE_POISSON","Imagick::CHANNEL_ALL"]}]', 'addNoise', 0, NULL, 1),
(31, 'blackThreshold', '[{"blackThreshold":["#3040a0"]}]', 'blackThreshold', 0, NULL, 1),
(32, 'charcoal', '[{"charcoal":[5,1]}]', 'charcoal', 0, NULL, 1),
(33, 'convolve', '[{"convolve":[]}]', 'convolve', 0, NULL, 1),
(34, 'equalize', '[{"equalize":[]}]', 'equalize', 0, NULL, 1),
(35, 'getPixel', '[{"getPixel":[]}]', 'getPixel', 0, NULL, 1),
(36, 'normalize', '[{"normalize":[]}]', 'normalize', 0, NULL, 1),
(37, 'oilPaint', '[{"oilPaint":[3]}]', 'oilPaint', 0, NULL, 1),
(38, 'orderedPosterize', '[{"orderedPosterize":[''o4x4'',"Imagick::CHANNEL_ALL"]}]', 'orderedPosterize', 0, NULL, 1),
(39, 'segment', '[{"segment":["Imagick::COLOR_CYAN",5,5]}]', 'segment', 0, NULL, 1),
(40, 'shear', '[{"shear":["#5c7f7f",10,5]}]', 'shear', 0, NULL, 1),
(41, 'sketch', '[{"sketch":[5,1,45]}]', 'sketch', 0, NULL, 1),
(42, 'spread', '[{"spread":[1]}]', 'spread', 0, NULL, 1),
(43, 'swirl', '[{"swirl":[50]}]', 'swirl', 0, NULL, 1),
(44, 'sigmoidalContrast', '[{"sigmoidalContrast":[1,3.,0.6,"Imagick::CHANNEL_ALL"]}]', 'sigmoidalContrast', 0, NULL, 1),
(45, 'texture_0001', '[{"textureOverlaying":["idp=6"]}]', 'texture_0001', 0, NULL, 4),
(46, 'texture_0002', '[{"textureOverlaying":["idp=7"]}]', 'texture_0002', 0, NULL, 4),
(47, 'texture_0003', '[{"textureOverlaying":["idp=8"]}]', 'texture_0003', 0, NULL, 4),
(48, 'texture_0004', '[{"textureOverlaying":["idp=9"]}]', 'texture_0004', 0, NULL, 4),
(49, 'texture_0005', '[{"textureOverlaying":["idp=10"]}]', 'texture_0005', 0, NULL, 4),
(50, 'texture_0006', '[{"textureOverlaying":["idp=11"]}]', 'texture_0006', 0, NULL, 4),
(51, 'texture_0007', '[{"textureOverlaying":["idp=12"]}]', 'texture_0007', 0, NULL, 4),
(52, 'texture_0008', '[{"textureOverlaying":["idp=13"]}]', 'texture_0008', 0, NULL, 4),
(53, 'texture_0009', '[{"textureOverlaying":["idp=14"]}]', 'texture_0009', 0, NULL, 4),
(54, 'texture_0010', '[{"textureOverlaying":["idp=15"]}]', 'texture_0010', 0, NULL, 4),
(55, 'texture_0011', '[{"textureOverlaying":["idp=16"]}]', 'texture_0011', 0, NULL, 4),
(56, 'texture_0012', '[{"textureOverlaying":["idp=17"]}]', 'texture_0012', 0, NULL, 4),
(57, 'texture_0013', '[{"textureOverlaying":["idp=18"]}]', 'texture_0013', 0, NULL, 4),
(58, 'texture_0014', '[{"textureOverlaying":["idp=19"]}]', 'texture_0014', 0, NULL, 4),
(59, 'texture_0015', '[{"textureOverlaying":["idp=20"]}]', 'texture_0015', 0, NULL, 4),
(60, 'texture_0016', '[{"textureOverlaying":["idp=21"]}]', 'texture_0016', 0, NULL, 4),
(61, 'texture_0017', '[{"textureOverlaying":["idp=22"]}]', 'texture_0017', 0, NULL, 4),
(62, 'texture_0018', '[{"textureOverlaying":["idp=23"]}]', 'texture_0018', 0, NULL, 4),
(63, 'texture_0019', '[{"textureOverlaying":["idp=24"]}]', 'texture_0019', 0, NULL, 4),
(64, 'texture_0020', '[{"textureOverlaying":["idp=25"]}]', 'texture_0020', 0, NULL, 4),
(65, 'texture_0021', '[{"textureOverlaying":["idp=26"]}]', 'texture_0021', 0, NULL, 4),
(66, 'texture_0022', '[{"textureOverlaying":["idp=27"]}]', 'texture_0022', 0, NULL, 4),
(67, 'texture_0023', '[{"textureOverlaying":["idp=28"]}]', 'texture_0023', 0, NULL, 4),
(68, 'texture_0024', '[{"textureOverlaying":["idp=29"]}]', 'texture_0024', 0, NULL, 4),
(69, 'texture_0025', '[{"textureOverlaying":["idp=30"]}]', 'texture_0025', 0, NULL, 4),
(70, 'texture_0026', '[{"textureOverlaying":["idp=31"]}]', 'texture_0026', 0, NULL, 4),
(71, 'texture_0027', '[{"textureOverlaying":["idp=32"]}]', 'texture_0027', 0, NULL, 4),
(72, 'texture_0028', '[{"textureOverlaying":["idp=33"]}]', 'texture_0028', 0, NULL, 4),
(73, 'texture_0029', '[{"textureOverlaying":["idp=34"]}]', 'texture_0029', 0, NULL, 4),
(74, 'cloth_0001', '[{"textureOverlaying":["idp=35"]}]', 'cloth_texture_0001', 0, NULL, 4),
(75, 'paper_0001', '[{"textureOverlaying":["idp=36"]}]', 'paper_texture_0001', 0, NULL, 4),
(76, 'paper_0002', '[{"textureOverlaying":["idp=37"]}]', 'paper_texture_0002', 0, NULL, 4),
(77, 'paper_0003', '[{"textureOverlaying":["idp=38"]}]', 'paper_texture_0003', 0, NULL, 4),
(78, 'paper_0004', '[{"textureOverlaying":["idp=39"]}]', 'paper_texture_0004', 0, NULL, 4),
(79, 'paper_0005', '[{"textureOverlaying":["idp=40"]}]', 'paper_texture_0005', 0, NULL, 4),
(80, 'paper_0006', '[{"textureOverlaying":["idp=41"]}]', 'paper_texture_0006', 0, NULL, 4),
(81, 'paper_0007', '[{"textureOverlaying":["idp=42"]}]', 'paper_texture_0007', 0, NULL, 4),
(82, 'paper_0008', '[{"textureOverlaying":["idp=43"]}]', 'paper_texture_0008', 0, NULL, 4),
(83, 'paper_0009', '[{"textureOverlaying":["idp=44"]}]', 'paper_texture_0009', 0, NULL, 4),
(84, 'paper_0010', '[{"textureOverlaying":["idp=45"]}]', 'paper_texture_0010', 0, NULL, 4),
(85, 'paper_0011', '[{"textureOverlaying":["idp=46"]}]', 'paper_texture_0011', 0, NULL, 4),
(86, 'paper_0012', '[{"textureOverlaying":["idp=47"]}]', 'paper_texture_0012', 0, NULL, 4),
(87, 'paper_0013', '[{"textureOverlaying":["idp=48"]},{"textureOverlaying":["idp=48"]}]', 'paper_texture_0013', 0, NULL, 4),
(88, 'paper_0014', '[{"textureOverlaying":["idp=49"]}]', 'paper_texture_0014', 0, NULL, 4),
(89, 'paper_0015', '[{"textureOverlaying":["idp=50"]}]', 'paper_texture_0015', 0, NULL, 4),
(90, 'paper_0016', '[{"textureOverlaying":["idp=51"]},{"textureOverlaying":["idp=51"]}]', 'paper_texture_0016', 0, NULL, 4),
(91, 'paper_0017', '[{"textureOverlaying":["idp=52"]}]', 'paper_texture_0017', 0, NULL, 4),
(92, 'paper_0018', '[{"textureOverlaying":["idp=53"]}]', 'paper_texture_0018', 0, NULL, 4),
(93, 'paper_0019', '[{"textureOverlaying":["idp=54"]}]', 'paper_texture_0019', 0, NULL, 4),
(94, 'paper_0020', '[{"textureOverlaying":["idp=55"]}]', 'paper_texture_0020', 0, NULL, 4),
(95, 'stone_0001', '[{"textureOverlaying":["idp=56"]}]', 'stone_texture_0001', 0, NULL, 4),
(96, 'stone_0002', '[{"textureOverlaying":["idp=57"]}]', 'stone_texture_0002', 0, NULL, 4),
(97, 'frame_0001', '[{"frameOverlaying":["idp=58"]}]', 'frame_digital_0001', 0, NULL, 3),
(98, 'frame_0002', '[{"frameOverlaying":["idp=59"]}]', 'frame_digital_0002', 0, NULL, 3),
(99, 'frame_0003', '[{"frameOverlaying":["idp=60"]}]', 'frame_digital_0003', 0, NULL, 3),
(100, 'frame_0004', '[{"frameOverlaying":["idp=61"]}]', 'frame_digital_0004', 0, NULL, 3),
(101, 'frame_0005', '[{"frameOverlaying":["idp=62"]}]', 'frame_digital_0005', 0, NULL, 3),
(102, 'paper_0001', '[{"frameOverlaying":["idp=63"]}]', 'paper_frame_0001', 0, NULL, 3),
(103, 'paper_0002', '[{"frameOverlaying":["idp=64"]}]', 'paper_frame_0002', 0, NULL, 3),
(104, 'paper_0003', '[{"frameOverlaying":["idp=65"]}]', 'paper_frame_0003', 0, NULL, 3),
(105, 'paper_0004', '[{"frameOverlaying":["idp=66"]}]', 'paper_frame_0004', 0, NULL, 3),
(106, 'paper_0005', '[{"frameOverlaying":["idp=67"]}]', 'paper_frame_0005', 0, NULL, 3),
(107, 'grunge_0001', '[{"frameOverlaying":["idp=1"]}]', 'grunge_frame_0001', 0, NULL, 3),
(108, 'grunge_0002', '[{"frameOverlaying":["idp=2"]}]', 'grunge_frame_0002', 0, NULL, 3),
(109, 'grunge_0003', '[{"frameOverlaying":["idp=3"]}]', 'grunge_frame_0003', 0, NULL, 3),
(110, 'grunge_0004', '[{"frameOverlaying":["idp=4"]}]', 'grunge_frame_0004', 0, NULL, 3),
(111, 'wooden_0001', '[{"frameOverlaying":["idp=5"]}]', 'wooden_frame_0001', 0, NULL, 3),
(112, 'wooden_0002', '[{"frameOverlaying":["idp=68"]}]', 'wooden_frame_0002', 0, NULL, 3),
(113, 'wooden_0003', '[{"frameOverlaying":["idp=69"]}]', 'wooden_frame_0003', 0, NULL, 3),
(114, 'wooden_0004', '[{"frameOverlaying":["idp=70"]}]', 'wooden_frame_0004', 0, NULL, 3),
(115, 'wooden_0005', '[{"frameOverlaying":["idp=71"]}]', 'wooden_frame_0005', 0, NULL, 3),
(116, 'frame_0006', '[{"frameOverlaying":["idp=82"]}]', 'frame_digital_0006', 0, NULL, 3),
(117, 'frame_0007', '[{"frameOverlaying":["idp=83"]}]', 'frame_digital_0007', 0, NULL, 3),
(118, 'frame_0008', '[{"frameOverlaying":["idp=84"]}]', 'frame_digital_0008', 0, NULL, 3),
(119, 'frame_0009', '[{"frameOverlaying":["idp=85"]}]', 'frame_digital_0009', 0, NULL, 3),
(120, 'frame_0010', '[{"frameOverlaying":["idp=86"]}]', 'frame_digital_0010', 0, NULL, 3),
(121, 'frame_0011', '[{"frameOverlaying":["idp=87"]}]', 'frame_digital_0011', 0, NULL, 3),
(122, 'frame_0012', '[{"frameOverlaying":["idp=88"]}]', 'frame_digital_0012', 0, NULL, 3),
(123, 'frame_0013', '[{"frameOverlaying":["idp=89"]}]', 'frame_digital_0013', 0, NULL, 3),
(124, 'frame_0014', '[{"frameOverlaying":["idp=90"]}]', 'frame_digital_0014', 0, NULL, 3),
(125, 'frame_0015', '[{"frameOverlaying":["idp=91"]}]', 'frame_digital_0015', 0, NULL, 3),
(126, 'frame_0016', '[{"frameOverlaying":["idp=92"]}]', 'frame_digital_0016', 0, NULL, 3),
(127, 'frame_0017', '[{"frameOverlaying":["idp=93"]}]', 'frame_digital_0017', 0, NULL, 3),
(128, 'frame_0018', '[{"frameOverlaying":["idp=94"]}]', 'frame_digital_0018', 0, NULL, 3),
(129, 'frame_0019', '[{"frameOverlaying":["idp=95"]}]', 'frame_digital_0019', 0, NULL, 3),
(130, 'frame_0020', '[{"frameOverlaying":["idp=96"]}]', 'frame_digital_0020', 0, NULL, 3),
(131, 'blurVignette', '[{"vignetteOverlaying":["idp=72"]}]', 'blurVignette', 0, NULL, 5),
(132, 'bordersVignette', '[{"vignetteOverlaying":["idp=73"]}]', 'bordersVignette', 0, NULL, 5),
(133, 'CLVignette', '[{"vignetteOverlaying":["idp=74"]}]', 'CLVignette', 0, NULL, 5),
(134, 'CVignette', '[{"vignetteOverlaying":["idp=75"]}]', 'CVignette', 0, NULL, 5),
(135, 'elipseVignette', '[{"vignetteOverlaying":["idp=76"]}]', 'elipseVignette', 0, NULL, 5),
(136, 'filterVignette', '[{"vignetteOverlaying":["idp=77"]}]', 'filterVignette', 0, NULL, 5),
(137, 'fullPageVignette', '[{"vignetteOverlaying":["idp=78"]}]', 'fullPageVignette', 0, NULL, 5),
(138, 'grayVignette', '[{"vignetteOverlaying":["idp=79"]}]', 'grayVignette', 0, NULL, 5),
(139, 'roundVignette', '[{"vignetteOverlaying":["idp=80"]}]', 'roundVignette', 0, NULL, 5),
(140, 'squareVignette', '[{"vignetteOverlaying":["idp=81"]}]', 'squareVignette', 0, NULL, 5),
(141, 'toaster', '[{"idf=15":[]},{"idf=16":[80.]},{"idf=111":[]}]', 'toaster', 0, NULL, 2),
(142, 'curlPaperNoise', '[{"idf=30":[]},{"idf=103":[]},{"idf=16":[]}]', 'curlPaperNoise', 0, NULL, 2),
(143, 'charcoalPaper', '[{"idf=32":[1,1]},{"idf=105":[]}]', 'charcoalPaper', 0, NULL, 2),
(144, 'radialBlurF', '[{"idf=28":[]},{"idf=115":[]},{"idf=14":[]},{"idf=16":[]}]', 'radialBlurF', 0, NULL, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `overlaying_image`
--

CREATE TABLE IF NOT EXISTS `overlaying_image` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `path` varchar(255) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=82 ;

--
-- Дамп данных таблицы `overlaying_image`
--

INSERT INTO `overlaying_image` (`id`, `name`, `path`, `description`) VALUES
(1, 'grunge_0001', 'frames/grunge/grunge_frame_0001.png', NULL),
(2, 'grunge_0002', 'frames/grunge/grunge_frame_0002.png', NULL),
(3, 'grunge_0003', 'frames/grunge/grunge_frame_0003.png', NULL),
(4, 'grunge_0004', 'frames/grunge/grunge_frame_0004.png', NULL),
(5, 'wooden_0001', 'frames/wooden/wooden_frame_0001.png', NULL),
(6, 'texture_0001', 'textures/texture/texture_0001.jpg', NULL),
(7, 'texture_0002', 'textures/texture/texture_0002.jpg', NULL),
(8, 'texture_0003', 'textures/texture/texture_0003.jpg', NULL),
(9, 'texture_0004', 'textures/texture/texture_0004.jpg', NULL),
(10, 'texture_0005', 'textures/texture/texture_0005.jpg', NULL),
(11, 'texture_0006', 'textures/texture/texture_0006.jpg', NULL),
(12, 'texture_0007', 'textures/texture/texture_0007.jpg', NULL),
(13, 'texture_0008', 'textures/texture/texture_0008.jpg', NULL),
(14, 'texture_0009', 'textures/texture/texture_0009.jpg', NULL),
(15, 'texture_0010', 'textures/texture/texture_0010.jpg', NULL),
(16, 'texture_0011', 'textures/texture/texture_0011.jpg', NULL),
(17, 'texture_0012', 'textures/texture/texture_0012.jpg', NULL),
(18, 'texture_0013', 'textures/texture/texture_0013.jpg', NULL),
(19, 'texture_0014', 'textures/texture/texture_0014.jpg', NULL),
(20, 'texture_0015', 'textures/texture/texture_0015.jpg', NULL),
(21, 'texture_0016', 'textures/texture/texture_0016.jpg', NULL),
(22, 'texture_0017', 'textures/texture/texture_0017.jpg', NULL),
(23, 'texture_0018', 'textures/texture/texture_0018.jpg', NULL),
(24, 'texture_0019', 'textures/texture/texture_0019.jpg', NULL),
(25, 'texture_0020', 'textures/texture/texture_0020.jpg', NULL),
(26, 'texture_0021', 'textures/texture/texture_0021.jpg', NULL),
(27, 'texture_0022', 'textures/texture/texture_0022.jpg', NULL),
(28, 'texture_0023', 'textures/texture/texture_0023.jpg', NULL),
(29, 'texture_0024', 'textures/texture/texture_0024.jpg', NULL),
(30, 'texture_0025', 'textures/texture/texture_0025.jpg', NULL),
(31, 'texture_0026', 'textures/texture/texture_0026.jpg', NULL),
(32, 'texture_0027', 'textures/texture/texture_0027.jpg', NULL),
(33, 'texture_0028', 'textures/texture/texture_0028.jpg', NULL),
(34, 'texture_0029', 'textures/texture/texture_0029.jpg', NULL),
(35, 'cloth_0001', 'textures/cloth/cloth_texture_0001.jpg', NULL),
(36, 'paper_0001', 'textures/paper/paper_texture_0001.jpg', NULL),
(37, 'paper_0002', 'textures/paper/paper_texture_0002.jpg', NULL),
(38, 'paper_0003', 'textures/paper/paper_texture_0003.jpg', NULL),
(39, 'paper_0004', 'textures/paper/paper_texture_0004.jpg', NULL),
(40, 'paper_0005', 'textures/paper/paper_texture_0005.jpg', NULL),
(41, 'paper_0006', 'textures/paper/paper_texture_0006.jpg', NULL),
(42, 'paper_0007', 'textures/paper/paper_texture_0007.jpg', NULL),
(43, 'paper_0008', 'textures/paper/paper_texture_0008.jpg', NULL),
(44, 'paper_0009', 'textures/paper/paper_texture_0009.jpg', NULL),
(45, 'paper_0010', 'textures/paper/paper_texture_0010.jpg', NULL),
(46, 'paper_0011', 'textures/paper/paper_texture_0011.jpg', NULL),
(47, 'paper_0012', 'textures/paper/paper_texture_0012.jpg', NULL),
(48, 'paper_0013', 'textures/paper/paper_texture_0013.jpg', NULL),
(49, 'paper_0014', 'textures/paper/paper_texture_0014.jpg', NULL),
(50, 'paper_0015', 'textures/paper/paper_texture_0015.jpg', NULL),
(51, 'paper_0016', 'textures/paper/paper_texture_0016.jpg', NULL),
(52, 'paper_0017', 'textures/paper/paper_texture_0017.jpg', NULL),
(53, 'paper_0018', 'textures/paper/paper_texture_0018.jpg', NULL),
(54, 'paper_0019', 'textures/paper/paper_texture_0019.jpg', NULL),
(55, 'paper_0020', 'textures/paper/paper_texture_0020.jpg', NULL),
(56, 'stone_0001', 'textures/stone/stone_texture_0001.jpg', NULL),
(57, 'stone_0002', 'textures/stone/stone_texture_0002.jpg', NULL),
(58, 'frame_0001', 'frames/digital/frame_0001.png', NULL),
(59, 'frame_0002', 'frames/digital/frame_0002.png', NULL),
(60, 'frame_0003', 'frames/digital/frame_0003.png', NULL),
(61, 'frame_0004', 'frames/digital/frame_0004.png', NULL),
(62, 'frame_0005', 'frames/digital/frame_0005.png', NULL),
(63, 'paper_0001', 'frames/paper/paper_frame_0001.png', NULL),
(64, 'paper_0002', 'frames/paper/paper_frame_0002.png', NULL),
(65, 'paper_0003', 'frames/paper/paper_frame_0003.png', NULL),
(66, 'paper_0004', 'frames/paper/paper_frame_0004.png', NULL),
(67, 'paper_0005', 'frames/paper/paper_frame_0005.png', NULL),
(68, 'wooden_0002', 'frames/wooden/wooden_frame_0002.png', NULL),
(69, 'wooden_0003', 'frames/wooden/wooden_frame_0003.png', NULL),
(70, 'wooden_0004', 'frames/wooden/wooden_frame_0004.png', NULL),
(71, 'wooden_0005', 'frames/wooden/wooden_frame_0005.png', NULL),
(72, 'blurVignette', 'vignettes/blurVignette.jpg', NULL),
(73, 'bordersVignette', 'vignettes/bordersVignette.jpg', NULL),
(74, 'CLVignette', 'vignettes/CLVignette.jpg', NULL),
(75, 'CVignette', 'vignettes/CVignette.jpg', NULL),
(76, 'elipseVignette', 'vignettes/elipseVignette.jpg', NULL),
(77, 'filterVignette', 'vignettes/filterVignette.jpg', NULL),
(78, 'fullPageVignette', 'vignettes/fullPageVignette.jpg', NULL),
(79, 'grayVignette', 'vignettes/grayVignette.jpg', NULL),
(80, 'roundVignette', 'vignettes/roundVignette.jpg', NULL),
(81, 'squareVignette', 'vignettes/squareVignette.jpg', NULL),
(82, 'frame_0001', 'frames/digital/frame_0006.png', NULL),
(83, 'frame_0002', 'frames/digital/frame_0007.png', NULL),
(84, 'frame_0003', 'frames/digital/frame_0008.png', NULL),
(85, 'frame_0004', 'frames/digital/frame_0009.png', NULL),
(86, 'frame_0005', 'frames/digital/frame_0010.png', NULL),
(87, 'frame_0005', 'frames/digital/frame_0011.png', NULL),
(88, 'frame_0005', 'frames/digital/frame_0012.png', NULL),
(89, 'frame_0005', 'frames/digital/frame_0013.png', NULL),
(90, 'frame_0005', 'frames/digital/frame_0014.png', NULL),
(91, 'frame_0005', 'frames/digital/frame_0015.png', NULL),
(92, 'frame_0005', 'frames/digital/frame_0016.png', NULL),
(93, 'frame_0005', 'frames/digital/frame_0017.png', NULL),
(94, 'frame_0005', 'frames/digital/frame_0018.png', NULL),
(95, 'frame_0005', 'frames/digital/frame_0019.png', NULL),
(96, 'frame_0005', 'frames/digital/frame_0020.png', NULL);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `filter`
--
ALTER TABLE `filter`
  ADD CONSTRAINT `filter_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
