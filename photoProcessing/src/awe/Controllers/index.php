<?php

Class Controller_Index Extends Controller_Base
{
    public $layouts = "first_layouts";

    function index()
    {
        $select = array(
            'where' => "owner is null", // условие
        );

        $model = new Model_Category($select); // создаем объект модели
        $categories = $model->getAllRows(); // получаем все строки

        $this->template->vars('categories', $categories);
        $this->template->view('index');
    }
}