<?php
session_start();
$original = 'original.jpg';
?>

    <link href="../../../web/css/style.css" rel="stylesheet" type="text/css">
    <script src="../../../web/js/jquery.js"></script>
    <script src="../../../web/js/script.js"></script>

<?php
include("functions.php");

$extensions = array('image/jpeg', 'image/jpg', 'image/png', 'image/gif');
$max_size = 512000; //$max_size = 2048000;

if ($_FILES['img']['name'] != "" && $_SERVER['REQUEST_METHOD'] === 'POST') {
    $tmp = $_FILES['img']['tmp_name'];
    $type = $_FILES['img']['type'];
    $new_name = time() . "_" . $_FILES['img']['name'];
    $pathToSessionTemp = '../../../temp/' . $_COOKIE['SID'] . '/';

    if (in_array($type, $extensions)) {
        if ($_FILES['img']['size'] < $max_size) {
            if (move_uploaded_file($tmp, $pathToSessionTemp . $new_name)) {
                copy($pathToSessionTemp . $new_name, $pathToSessionTemp . $original);
                echo '<img src="' . $pathToSessionTemp . $new_name . '" class="thumb" width="200">';
                echo '<script>
						set_name("' . $new_name . '");
					  </script>';
                unset($_SESSION['imgArray']);
                $_SESSION['imageIterator'] = 0;
                $_SESSION['imgArray'][$_SESSION['imageIterator']] = $new_name;
            } else {
                echo err("Unexpected error");
            }
        } else {
            echo err("Your file is too large");
        }
    } else {
        echo err("Invalid image file");
    }
} else {
    echo err("Please select file to upload");
}
?>