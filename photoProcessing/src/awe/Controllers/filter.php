<?php

Class Controller_Filter Extends Controller_Base
{
    public $layouts = "first_layouts";

    function index()
    {
        $idFilter = (isset($_GET['id'])) ? (int)$_GET['id'] : false;
        if ($idFilter) {

            $select = array(
                'where' => "id = $idFilter" // условие
            );
            $model = new Model_Filter($select); // создаем объект модели
            $filter = $model->getOneRow(); // получаем фильтр

            $pattern = '/"id=\d+"/'; // паттерн для поиска выраженя "id=???"
            preg_match_all($pattern, $filter['settings'], $matches); // сбор совпадений с паттерном
            $pattern = '/\d+/';
            preg_match_all($pattern, json_encode($matches), $matches); // сбор только номеров айдишников

            $tempSelect = '';

            //создание запросов только для нунжных айди
            for ($i = 0; $i < count($matches[0]); ++$i) {
                if ($i !== 0) {
                    $tempSelect .= ' OR ';
                }
                $tempSelect .= "id = {$matches[0][$i]}";
            }

            $adittionsSourse = '';

            //вытаскивание из БД нужных полей (дополнитеьлные ресурсы изображений)
            if ('' != $tempSelect) {
                $selectForOverlayingImage = ['where' => $tempSelect];
                $modelOverlayingImage = new Model_Overlaying_Image($selectForOverlayingImage);
                $adittionsSourse = $modelOverlayingImage->getAllRows();

                //замена айдишников дополнительных ресурсов на прямой путь
                for ($i = 0; $i < count($adittionsSourse); ++$i) {
                    $search = '"id=' . $adittionsSourse[$i]['id'] . '"';
                    $replace = json_encode(realpath(BASE_RESOURSE_PATH . $adittionsSourse[$i]['path']));
                    $filter['settings'] = str_replace($search, $replace, $filter['settings']);
                }
            }


        } else {
            $filter = false;
            $adittionsSourse = false;
        }

        $this->template->vars('filter', $filter);
        $this->template->vars('adittionsSourse', $adittionsSourse);
        $this->template->view('index');
    }

}