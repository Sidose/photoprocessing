<?php

class FilterFactory
{

    /**
     * @var \Imagick
     */
    private $imagick;

    /**
     * @var string
     */
    private $extension;

    /**
     * The FilterFactory constructor
     * @param mixed $imagePath
     * The path to an image to load or an array of paths. Paths can include
     * wildcards for file names, or can be URLs.
     */
    public function __construct($imagePath)
    {
//        try {
        $this->imagick = new Imagick(realpath($imagePath));
//            $this->imagick = new Imagick($imagePath);
        $this->extension = pathinfo($imagePath, PATHINFO_EXTENSION);
//        } catch (ImagickException $e) {
//            echo 'Error: ', $e->getMessage();
//            die();
//        }
    }

    /**
     * @return \Imagick
     */
    public function getImagick()
    {
        return $this->imagick;
    }

    /**
     * @param \Imagick $imagick
     */
    public function setImagick($imagick)
    {
        $this->imagick = $imagick;
    }

    /**
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }


    ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////
    ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////
    ///// ///// ///// ///// /////                                           ///// ///// ///// ///// /////
    ///// ///// ///// ///// /////             basic filters block           ///// ///// ///// ///// /////
    ///// ///// ///// ///// /////                                           ///// ///// ///// ///// /////
    ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////
    ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////

    ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////
    ///// ///// ///// ///// ///// sharpen  category ///// ///// ///// ///// /////

    /**
     * Sharpens an image
     * @link http://php.net/manual/en/imagick.sharpenimage.php <br/>
     * @param float $radius [optional] <br/>
     * @param float $sigma [optional] <br/>
     * @param int | string $channel [optional] <br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function sharpen($radius = 2., $sigma = 1., $channel = Imagick::CHANNEL_ALL)
    {
        $channel = $this->getConstantFromString($channel);
        if (is_numeric($radius) && is_numeric($sigma) && is_numeric($channel)) {
            return $this->imagick->sharpenimage(floatval($radius), floatval($sigma), floatval($channel));
        }

        return false;
    }

    /**
     * Adaptively sharpen the image<br/>
     * @link http://php.net/manual/en/imagick.adaptivesharpenimage.php <br/><br/>
     * @param float $radius [optional] <br/>
     * The radius of the Gaussian, in pixels, not counting the center pixel. Use 0 for auto-select.<br/><br/>
     * @param float $sigma [optional] <br/>
     * The standard deviation of the Gaussian, in pixels. <br/><br/>
     * @param int | string $channel [optional] <br/>
     * Provide any channel constant that is valid for your channel mode. To apply to more than one channel, combine channel constants using bitwise operators. Defaults to <b>Imagick::CHANNEL_DEFAULT</b>. Refer to this list of channel constants <br/><br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function adaptiveSharpen($radius = 3., $sigma = 1., $channel = Imagick::CHANNEL_ALL)
    {
        $channel = $this->getConstantFromString($channel);
        if (is_numeric($radius) && is_numeric($sigma) && is_numeric($channel)) {
            return $this->imagick->sharpenimage(floatval($radius), floatval($sigma), floatval($channel));
        }

        return false;
    }

    /**
     * Sharpens an image
     * @link http://php.net/manual/en/imagick.unsharpmaskimage.php
     * @param float $radius [optional]
     * @param float $sigma [optional]
     * @param float $amount [optional]
     * @param float $threshold [optional]
     * @param int | string $channel [optional]
     * @return bool <b>TRUE</b> on success.
     */
    public function unsharpMaskSharpen($radius = 3., $sigma = 1., $amount = 2., $threshold = 0., $channel = Imagick::CHANNEL_ALL)
    {
        $channel = $this->getConstantFromString($channel);
        if (is_numeric($radius) && is_numeric($sigma) && is_numeric($amount) && is_numeric($threshold) && is_numeric($channel)) {
            return $this->imagick->unsharpmaskimage(floatval($radius), floatval($sigma), floatval($amount), floatval($threshold), floatval($channel));
        }

        return false;
    }

    ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////


    ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////
    ///// ///// ///// ///// ///// contrast category ///// ///// ///// ///// /////

    /**
     * Change the contrast of the image <br/>
     * @link http://php.net/manual/en/imagick.contrastimage.php <br/>
     * @param int $sharpen [optional] - The sharpen value <br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function contrast($sharpen = 0)
    {
        if (is_numeric($sharpen)) {
            $tmp = true;
            $sharpen = -$sharpen;
            if (0 <= $sharpen) {
                $tmp = false;
            }

            for ($iter = abs($sharpen); $iter > 0; --$iter) {
                $this->imagick->contrastimage($tmp);
            }
            return true;
        }

        return false;
    }

    /**
     * @param int $black_point
     * @param int $white_point
     * @param int | string $channel
     * @return bool
     */
    public function contrastStretch($black_point = 50, $white_point = 50, $channel = Imagick::CHANNEL_ALL)
    {
        $channel = $this->getConstantFromString($channel);
        if (is_numeric($black_point) && is_numeric($white_point) && is_numeric($channel)) {
            return $this->imagick->contraststretchimage(floatval($black_point), floatval($white_point), floatval($channel));
        }

        return false;
    }

    /**
     * Adjusts the contrast of an image <br/>
     * @link http://php.net/manual/en/imagick.sigmoidalcontrastimage.php <br/>
     * @param int $sharpen [optional] <br/>
     * @param float $alpha [optional] The amount of contrast to apply. 1 is very little, 5 is a significant amount, 20 is extreme. <br/>
     * @param float $beta [optional] Where the midpoint of the gradient will be. This value should be in the range 0 to 1 - mutliplied by the quantum value for ImageMagick. <br/>
     * @param int | string $channel [optional] <br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function sigmoidalContrast($sharpen = 1, $alpha = 3., $beta = 0.5, $channel = Imagick::CHANNEL_ALL)
    {
        $channel = $this->getConstantFromString($channel);

        if (is_numeric($sharpen) && is_numeric($alpha) && is_numeric($beta) && is_numeric($channel)) {
            $tmp = true;
            if (0 >= $sharpen) {
                $tmp = false;
            }

            for ($iter = abs($sharpen); $iter > 0; --$iter) {
                $this->imagick->sigmoidalContrastImage($tmp, floatval($alpha), floatval($beta), floatval($channel));
            }

            return true;
        }

        return false;
    }

    ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////


    ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////
    ///// ///// ///// ///// ///// modulate category ///// ///// ///// ///// /////
    ///// ///// ///// /////  hue, brightness, saturation  ///// ///// ///// /////
    ///// /////  Each parameter can take values ranging from 0 to 200 ///// /////

    /**
     * Control the brightness, saturation, and hue
     * @link http://php.net/manual/en/imagick.modulateimage.php
     * @param float $brightness [optional] <br/>
     * @param float $saturation [optional] <br/>
     * @param float $hue [optional] <br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function modulate($brightness = 100., $saturation = 100., $hue = 100.)
    {
        if (is_numeric($brightness) && is_numeric($saturation) && is_numeric($hue)) {
            return $this->imagick->modulateimage(floatval($brightness), floatval($saturation), floatval($hue));
        }

        return false;
    }

    /**
     * Random brightness, saturation, and hue
     * @return bool
     */
    public function modulateRandom()
    {

        return $this->modulate(100, rand(30, 170), rand(50, 150));
    }

    /**
     * Control the hue
     * @param float $hue [optional] <br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function hue($hue = 100.)
    {

        return $this->modulate(100, 100, floatval($hue));
    }

    /**
     * Control the saturation
     * @param float $saturation [optional] <br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function saturation($saturation = 100.)
    {

        return $this->modulate(100, floatval($saturation), 100);
    }

    /**
     * Control the brightness
     * @param float $brightness [optional] <br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function brightness($brightness = 100.)
    {

        return $this->modulate(floatval($brightness), 100, 100);
    }

    /**
     * grayscale
     * @return bool <b>TRUE</b> on success.
     */
    public function grayscale()
    {

        return $this->modulate(100, 0, 100);
    }

    ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////


    ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////
    ///// ///// ///// ///// /////  filter category  ///// ///// ///// ///// /////
    ///// ///// ///// ///// / sepia, negative, recolor  / ///// ///// ///// /////
    /**
     * Negates the colors in the reference image
     * @link http://php.net/manual/en/imagick.negateimage.php
     * @param bool $gray [optional] - Whether to only negate grayscale pixels within the image. <br/>
     * @param int | string $channel [optional] -
     * Provide any channel constant that is valid for your channel mode. To
     * apply to more than one channel, combine channeltype constants using
     * bitwise operators. Refer to this list of channel constants. <br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function invert($gray = false, $channel = Imagick::CHANNEL_ALL)
    {
        $channel = $this->getConstantFromString($channel);
        if (is_bool($gray) && is_numeric($channel)) {
            return $this->imagick->negateImage($gray, $channel);
        }

        return false;
    }

    /**
     * Sepia tones an image
     * @link http://php.net/manual/en/imagick.sepiatoneimage.php
     * @param float $threshold [optional]
     * @return bool <b>TRUE</b> on success.
     */
    public function sepia($threshold = 80.)
    {
        if (is_float($threshold)) {
            return $this->imagick->sepiaToneImage($threshold);
        }

        return false;
    }

    /**
     * (No version information available, might only be in SVN)<br/>
     * Recolors image
     * @link http://php.net/manual/en/imagick.recolorimage.php
     * @param array $matrix - The matrix containing the color values <br/>
     * @return bool <b>TRUE</b> on success.
     */
    //TODO еще нужно разобраться
    public function recolor($matrix = array('0', '100', '0', '0'))
    {
        if (is_array($matrix)) {
            return $this->imagick->recolorImage($matrix);
        }

        return false;
    }

    ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////


    ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////
    ///// ///// ///// ///// // crop & resize  category // ///// ///// ///// /////

    /**
     * Extracts a region of the image
     * @link http://php.net/manual/en/imagick.cropimage.php
     * @param int $width [optional]- The width of the crop <br/>
     * @param int $height [optional] - The height of the crop <br/>
     * @param int $x [optional] - The X coordinate of the cropped region's top left corner <br/>
     * @param int $y [optional] - The Y coordinate of the cropped region's top left corner <br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function crop($width = 100, $height = 100, $x = 0, $y = 0)
    {
        if (is_numeric($width) && is_numeric($height) && is_numeric($x) && is_numeric($y)) {
            return $this->imagick->cropimage(floatval($width), floatval($height), floatval($x), floatval($y));
        }
        return false;
    }

    /**
     * Scales an image
     * @link http://php.net/manual/en/imagick.resizeimage.php
     * @param int $width - Width of the image <br/>
     * @param int $height - Height of the image <br/>
     * @param bool $bestfit [optional] - Optional fit parameter. <br/>
     * @param float $sharp [optional] - The blur factor where &gt; 1 is blurry, &lt; 1 is sharp. <br/>
     * @param int | string $filter [optional] - Refer to the list of filter constants. <br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function resize($width, $height, $bestfit = false, $sharp = 0.9, $filter = imagick::FILTER_LANCZOS)
    {
        $filter = $this->getConstantFromString($filter);
        if (is_numeric($width) && is_numeric($height) && is_numeric($sharp) && is_bool($bestfit)) {
            return $this->imagick->resizeimage($width, $height, $filter, $sharp, $bestfit);
        }

        return false;
    }

    /**
     * Creates a crop thumbnail
     * @link http://php.net/manual/en/imagick.cropthumbnailimage.php <br/>
     * @param int $width [optional] - The width of the thumbnail <br/>
     * @param int $height [optional] - The Height of the thumbnail <br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function cropThumbnail($width = 100, $height = 100)
    {
        if (is_numeric($width) && is_numeric($height)) {
            return $this->imagick->cropthumbnailimage(floatval($width), floatval($height));
        }

        return false;
    }

    ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////


    ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////
    ///// ///// ///// ///// /// overlapping category  /// ///// ///// ///// /////
    ///// ///// ///// ///  composite, change image opacity  /// ///// ///// /////
    ///// ///// frame superposition,  texture overlapping,  vignette ////// /////

    /**
     * Composite one image onto another
     * @link http://php.net/manual/en/imagick.compositeimage.php
     * @param FilterFactory $composite_object - FilterFactory object which holds the composite image <br/>
     * @param int | string $composite [optional]
     * @param int $x [optional] - The column offset of the composited image <br/>
     * @param int $y [optional] - The row offset of the composited image <br/>
     * @param int $channel [optional] - Provide any channel constant that is valid for your channel mode. To apply to more
     * than one channel, combine channeltype constants using bitwise operators. Refer to this list of channel constants. <br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function composite($composite_object, $composite = Imagick::COMPOSITE_DEFAULT, $x = 0, $y = 0, $channel = Imagick::CHANNEL_ALL)
    {
        $composite = $this->getConstantFromString($composite);
        if (isset($composite_object) && is_numeric($composite) && is_numeric($x) && is_numeric($y) && is_numeric($channel)) {
            $composite_object = new FilterFactory($composite_object);
            return $this->imagick->compositeImage($composite_object->getImagick(), $composite, $x, $y, $channel);
        }

        return false;
    }

    /**
     * Composite one image onto another
     * @link http://php.net/manual/en/imagick.compositeimage.php
     * @param string $composite_object - file path to composite image <br/>
     * @param int | string $composite [optional]
     * @param int [optional] $x - The column offset of the composited image <br/>
     * @param int [optional] $y - The row offset of the composited image <br/>
     * @param int $channel [optional] - Provide any channel constant that is valid for your channel mode. To
     * apply to more than one channel, combine channel type constants using
     * bitwise operators. Refer to this list of channel constants. <br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function textureOverlaying($composite_object, $composite = Imagick::COMPOSITE_OVERLAY, $x = 0, $y = 0, $channel = Imagick::CHANNEL_ALL)
    {
        $composite = $this->getConstantFromString($composite);
        if (is_string($composite_object) && is_numeric($composite) && is_numeric($x) && is_numeric($y) && is_numeric($channel)) {
            $composite_object = new FilterFactory($composite_object);
            $compositeGeo = $composite_object->getImagick()->getimagegeometry();
            $thisImageGeo = $this->imagick->getimagegeometry();
            // TODO продумать обрезку исходного изображения по соотношению сторон от текстуры или рамки
//            if (rand(1, 2) > 1) {
//                $composite_object->getImagick()->rotateimage('#00000000', 180);
//            }
            if ($thisImageGeo['width'] > $thisImageGeo['height'] && $compositeGeo['width'] < $compositeGeo['height']
                || $thisImageGeo['width'] < $thisImageGeo['height'] && $compositeGeo['width'] > $compositeGeo['height']
            ) {
                $composite_object->rotate(90, '#00000000');
            }

            $composite_object->resize($thisImageGeo['width'], $thisImageGeo['height']);
            return $this->imagick->compositeImage($composite_object->getImagick(), $composite, $x, $y, $channel);
        }

        return false;
    }

    /**
     * Composite one image onto another
     * @link http://php.net/manual/en/imagick.compositeimage.php
     * @param string $composite_object - file path to composite frame <br/>
     * @param int | string $composite [optional]
     * @param int $x [optional] - The column offset of the composited image <br/>
     * @param int $y [optional] - The row offset of the composited image <br/>
     * @param int $channel [optional] - Provide any channel constant that is valid for your channel mode. To apply to more
     * than one channel, combine channel type constants using bitwise operators. Refer to this list of channel constants. <br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function frameOverlaying($composite_object, $composite = Imagick::COMPOSITE_DEFAULT, $x = 0, $y = 0, $channel = Imagick::CHANNEL_ALL)
    {

        return $this->textureOverlaying($composite_object, $composite, $x, $y, $channel);
    }

    /**
     * Composite one image onto another
     * @link http://php.net/manual/en/imagick.compositeimage.php
     * @param string $composite_object - file path to composite frame <br/>
     * @param int | string $composite [optional]
     * @param int $x [optional] - The column offset of the composited image <br/>
     * @param int $y [optional] - The row offset of the composited image <br/>
     * @param int $channel [optional] - Provide any channel constant that is valid for your channel mode. To apply to more
     * than one channel, combine channel type constants using bitwise operators. Refer to this list of channel constants. <br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function vignetteOverlaying($composite_object, $composite = Imagick::COMPOSITE_MULTIPLY, $x = 0, $y = 0, $channel = Imagick::CHANNEL_ALL)
    {

        return $this->textureOverlaying($composite_object, $composite, $x, $y, $channel);
    }

    /**
     * Sets the image opacity level (changes format to png)
     * @link http://php.net/manual/en/imagick.setimageopacity.php
     * @param float $opacity [optional] - The level of transparency: 1.0 is fully opaque and 0.0 is fully <br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function setImageOpacity($opacity = 0.5)
    {
        if (is_numeric($opacity)) {
            $this->setFormat('png');
            return $this->imagick->setImageOpacity($opacity);
        }

        return false;
    }

    /**
     * Addsvignettefiltertotheimage
     * @link http://php.net/manual/en/imagick.vignetteimage.php
     * @param float $blackPoint [optional] - The black point. <br/>
     * @param float $whitePoint [optional] - The white point <br/>
     * @param int $x [optional] - X offset of the ellipse <br/>
     * @param int $y [optional] - Y off set of the ellipse <br/>
     * @return bool <b> TRUE </b> on success.
     */
    public function vignette($blackPoint = 30., $whitePoint = 30., $x = NULL, $y = NULL)
    {
        if (0 == $x) {
            $x = 50;
        }

        if (0 == $y) {
            $y = 50;
        }

        return $this->imagick->vignetteImage($blackPoint, $whitePoint, $x, $y);
    }


//TODO пока не работает - нужно доделать
    public function mask($mask_object, $x = 0, $y = 0)
    {
        $this->setFormat('png');
        $this->imagick->setImageMatte(1);
        $this->resize(549, 549);

        return  $this->composite($mask_object, Imagick::COMPOSITE_DSTIN, $x, $y, Imagick::CHANNEL_ALPHA);
    }

///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////


///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////
///// ///// ///// ///// ///  mirroring  category  /// ///// ///// ///// /////

    /**
     * Creates a horizontal mirror image
     * @link http://php.net/manual/en/imagick.flopimage.php
     * @return bool <b>TRUE</b> on success.
     */
    public function horizontalMirroring()
    {

        return $this->imagick->flopimage();
    }

    /**
     * Creates a vertical mirror image
     * @link http://php.net/manual/en/imagick.flipimage.php
     * @return bool <b>TRUE</b> on success.
     */
    public function verticalMirroring()
    {

        return $this->imagick->flipimage();
    }

///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////


///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////
///// ///// ///// /////  setFrame,    textureTiling  ///// ///// ///// /////

    /**
     * Adds a simulated three-dimensional border
     * @link http://php.net/manual/en/imagick.frameimage.php
     * @param mixed $matteColor [optional] - ImagickPixel object or a string representing the matte color <br/>
     * @param int $width [optional] - The width of the border <br/>
     * @param int $height [optional] - The height of the border <br/>
     * @param int $innerBevel [optional] - The inner bevel width <br/>
     * @param int $outerBevel [optional] - The outer bevel width <br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function setFrame($matteColor = "#aabbcc", $width = 20, $height = 20, $innerBevel = 5, $outerBevel = 5)
    {
        if (is_string($matteColor) && is_numeric($width) && is_numeric($height) && is_numeric($innerBevel) && is_numeric($outerBevel)) {
            return $this->imagick->frameImage($matteColor, $width, $height, $innerBevel, $outerBevel);
        }

        return false;
    }

    /**
     * Repeatedly tiles the texture image
     * @link http://php.net/manual/en/imagick.textureimage.php
     * @param Imagick $textureWand
     * @param float $opacity [optional]
     * @param int $width [optional]
     * @param int $height [optional]
     * @param bool $bestfit [optional] - Optional fit parameter.
     * @return bool <b>TRUE</b> on success.
     */
    public function textureTiling($textureWand, $opacity = 0.5, $width = 50, $height = 50, $bestfit = false)
    {
        if(is_numeric($opacity) && is_numeric($width) && is_numeric($height)){
            $texture = new Imagick($textureWand);
            $texture->resizeimage($width, $height, $bestfit, 0.9, imagick::FILTER_LANCZOS);
            $texture->setFormat('png');
            $texture->setimageopacity($opacity);
            return $this->imagick = $this->imagick->textureImage($texture);
        }

        return false;
    }

///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////
///// ///// ///// ///// ///// Blur category /// ///// ///// ///// ///// /////

    /**
     * Adds adaptive blur filter to image
     * @link http://php.net/manual/en/imagick.adaptiveblurimage.php
     * @param float $radius [optional] - The radius of the Gaussian, in pixels, not counting the center pixel.
     * Provide a value of 0 and the radius will be chosen automagically. <br/>
     * @param float $sigma [optional] - The standard deviation of the Gaussian, in pixels. <br/>
     * @param int | string $channel [optional] -
     * Provide any channel constant that is valid for your channel mode. To apply to more than one channel,
     * combine channel constants using bitwise operators. Defaults to <b>Imagick::CHANNEL_DEFAULT</b>.
     * Refer to this list of channel constants <br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function adaptiveBlur($radius = 5., $sigma = 3., $channel = Imagick::CHANNEL_DEFAULT)
    {
        $channel = $this->getConstantFromString($channel);
        if (is_numeric($radius) && is_numeric($sigma)) {
            return $this->imagick->adaptiveBlurImage($radius, $sigma, $channel);
        }

        return false;
    }

    /**
     * Adds blur filter to image
     * @link http://php.net/manual/en/imagick.blurimage.php
     * @param float $radius [optional] - Blur radius <br/>
     * @param float $sigma [optional] - Standard deviation <br/>
     * @param int | string $channel [optional] - The Channeltype constant. When not supplied, all channels are blurred. <br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function blur($radius = 5., $sigma = 3., $channel = Imagick::CHANNEL_DEFAULT)
    {
        $channel = $this->getConstantFromString($channel);
        if (is_numeric($radius) && is_numeric($sigma) && is_int($channel)) {
            return $this->imagick->blurImage($radius, $sigma, $channel);
        }

        return false;
    }

    /**
     * Blurs an image
     * @link http://php.net/manual/en/imagick.gaussianblurimage.php
     * @param float $radius - The radius of the Gaussian, in pixels, not counting the center pixel. <br/>
     * @param float $sigma - The standard deviation of the Gaussian, in pixels. <br/>
     * @param int | string $channel [optional] - Provide any channel constant that is valid for your channel mode. To
     * apply to more than one channel, combine channeltype constants using
     * bitwise operators. Refer to this list of channel constants. <br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function gaussianBlur($radius = 5., $sigma = 3., $channel = Imagick::CHANNEL_ALL)
    {
        $channel = $this->getConstantFromString($channel);
        if (is_numeric($radius) && is_numeric($sigma) && is_int($channel)) {
            return $this->imagick->gaussianBlurImage($radius, $sigma, $channel);
        }

        return false;
    }

    /**
     * Simulates motion blur
     * @link http://php.net/manual/en/imagick.motionblurimage.php
     * @param float $radius [optional] - The radius of the Gaussian, in pixels, not counting the center pixel. <br/>
     * @param float $sigma [optional] - The standard deviation of the Gaussian, in pixels. <br/>
     * @param float $angle [optional] - Apply the effect along this angle. <br/>
     * @param int | string $channel [optional] <p>
     * Provide any channel constant that is valid for your channel mode. To apply to more than one channel,
     * combine channeltype constants using bitwise operators. Refer to this list of channel constants.
     * The channel argument affects only if Imagick is compiled against ImageMagick version
     * 6.4.4 or greater. <br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function motionBlur($radius = 5., $sigma = 3., $angle = 90., $channel = Imagick::CHANNEL_ALL)
    {
        $channel = $this->getConstantFromString($channel);
        if (is_numeric($radius) && is_numeric($sigma) && is_numeric($angle) && is_int($channel)) {
            return $this->imagick->motionBlurImage($radius, $sigma, $angle, $channel);
        }

        return false;
    }

    /**
     * Radial blurs an image
     * @link http://php.net/manual/en/imagick.radialblurimage.php
     * @param float $angle <br/>
     * @param int | string $channel [optional] <br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function radialBlur($angle, $channel = Imagick::CHANNEL_ALL)
    {
        $channel = $this->getConstantFromString($channel);
        if (is_numeric($angle) && is_int($channel)) {
            return $this->imagick->radialBlurImage($angle, $channel);
        }

        return false;
    }

///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////
///// ///// ///// ///// /// emboss category /// ///// ///// ///// ///// /////

    /**
     * Returns a grayscale image with a three-dimensional effect
     * @link http://php.net/manual/en/imagick.embossimage.php
     * @param float $radius [optional] - The radius of the effect <br/>
     * @param float $sigma [optional] - The sigma of the effect <br/>
     * @return bool <b>TRUE</b> on success.
     */
    function emboss($radius = 0., $sigma = 1.)
    {
        if (is_numeric($radius) && is_numeric($sigma)) {
            return $this->imagick->embossImage($radius, $sigma);
        }

        return false;
    }

///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////


///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////
///// ///// ///// / setFormat rotate getOrientation / ///// ///// ///// /////
    /**
     * Sets the format of the Imagick object
     * @link http://php.net/manual/en/imagick.setformat.php
     * @param string $format
     * @return bool <b>TRUE</b> on success.
     */
    public function setFormat($format = 'jpg')
    {
        $extensions = array('image/jpeg', 'image/jpg', 'image/png', 'image/gif', 'image/tif', 'image/tiff');
        if (is_string($format) && in_array($format, $extensions)) {
            $this->extension = $format;
            return $this->imagick->setFormat($this->extension);
        }

        return false;
    }

    /**
     * Rotates an image
     * @link http://php.net/manual/en/imagick.rotateimage.php
     * @param mixed $background - The background color <br/>
     * @param float $degrees - The number of degrees to rotate the image <br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function rotate($degrees = 0., $background = '#00000000')
    {
        if (is_numeric($degrees) && is_string($background)) {
            return $this->imagick->rotateimage($background, $degrees);
        }

        return false;
    }

    /**
     * Gets the image orientation
     * @link http://php.net/manual/en/imagick.getimageorientation.php
     * @return int an int on success.
     */
    public function orientation()
    {

        return $orientation = $this->imagick->getImageOrientation();
    }

///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// New methods
///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// New methods
///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// New methods
///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// New methods
///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// New methods


    /**
     * (No version information available, might only be in SVN)<br/>
     * Segments an image
     * @link http://php.net/manual/en/imagick.segmentimage.php
     * @param int | string $COLORSPACE - One of the COLORSPACE constants.<br/>
     * @param float $clusterThreshold [optional] - A percentage describing minimum number of pixels contained in hexedra before it is considered valid.<br/>
     * @param float $smoothThreshold [optional] - Eliminates noise from the histogram.<br/>
     * @param bool $verbose [optional] - Whether to output detailed information about recognised classes.<br/>
     * @return bool
     */
    public function segment($COLORSPACE = imagick::COLOR_CYAN, $clusterThreshold = 5., $smoothThreshold = 5., $verbose = false)
    {
        $COLORSPACE = $this->getConstantFromString($COLORSPACE);
        if (is_numeric($COLORSPACE) && is_numeric($clusterThreshold) && is_numeric($smoothThreshold)) {
            return $this->imagick->segmentImage($COLORSPACE, $clusterThreshold, $smoothThreshold, $verbose);
        }

        return false;
    }


    /**
     * Adds random noise to the image
     * @link http://php.net/manual/en/imagick.addnoiseimage.php
     * @param int | string  $noiseType <br/>
     * The type of the noise. Refer to this list of noise constants. <br/>
     * @param int | string $channel [optional] <p>
     * Provide any channel constant that is valid for your channel mode. To apply to more than one channel, combine channel constants
     * using bitwise operators. Defaults to <b>Imagick::CHANNEL_DEFAULT</b>. Refer to this list of channel constants <br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function addNoise($noiseType = Imagick::NOISE_POISSON, $channel = Imagick::CHANNEL_ALL)
    {
        // NOISE_UNIFORM = 1; NOISE_GAUSSIAN = 2; NOISE_MULTIPLICATIVEGAUSSIAN = 3;
        // NOISE_IMPULSE = 4; NOISE_LAPLACIAN = 5; NOISE_POISSON = 6; NOISE_RANDOM = 7;red gren blue alfa
        //    $imagick = new \Imagick(realpath($imagePath));

        $noiseType = $this->getConstantFromString($noiseType);
        $channel = $this->getConstantFromString($channel);
        if(is_numeric($noiseType) && is_numeric($channel)){
            return $this->imagick->addNoiseImage($noiseType, $channel);
        }

        return false;
    }

    /**
     * Forces all pixels below the threshold into black
     * @link http://php.net/manual/en/imagick.blackthresholdimage.php
     * @param mixed $thresholdColor - The threshold below which everything turns black <br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function blackThreshold($thresholdColor = "#3040a0")
    {

        return $this->imagick->blackthresholdimage($thresholdColor);
    }

    /**
     * (PECL imagick 2.0.0)<br/>
     * Simulates a charcoal drawing
     * @link http://php.net/manual/en/imagick.charcoalimage.php
     * @param float $radius - The radius of the Gaussian, in pixels, not counting the center pixel <br/>
     * @param float $sigma - The standard deviation of the Gaussian, in pixels
     * @return bool <b>TRUE</b> on success.
     */
    public function charcoal($radius = 5., $sigma = 1.)
    {
        if(is_numeric($radius) && is_numeric($sigma)){
            return $this->imagick->charcoalImage($radius, $sigma);
        }

        return false;
    }

    /**
     * Simulates a pencil sketch
     * @link http://php.net/manual/en/imagick.sketchimage.php
     * @param float $radius - The radius of the Gaussian, in pixels, not counting the center pixel<br/>
     * @param float $sigma - The standard deviation of the Gaussian, in pixels.<br/>
     * @param float $angle - Apply the effect along this angle.<br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function sketch($radius = 5., $sigma = 1., $angle = 45.)
    { //5 1 45
        if (is_numeric($radius) && is_numeric($sigma) && is_numeric($angle)) {
            return $this->imagick->sketchimage($radius, $sigma, $angle);
        }

        return false;
    }

    /**
     * @param $xRounding [optional]
     * @param $yRounding [optional]
     * @param $strokeWidth [optional]
     * @param $displace [optional]
     * @param $sizeCorrection [optional]
     * @return bool|void
     */
    public function roundCorners($xRounding = 20, $yRounding = 20, $strokeWidth = 5, $displace = 0, $sizeCorrection = 0)
    {
        if (is_numeric($xRounding) && is_numeric($yRounding) && is_numeric($strokeWidth) && is_numeric($displace) && is_numeric($sizeCorrection)) {
            $this->imagick->setBackgroundColor('red');
            $this->imagick->setbackgroundcolor('pink');

            return $this->imagick->roundCornersImage(
                $xRounding,
                $yRounding,
                $strokeWidth,
                $displace,
                $sizeCorrection
            );
        }

        return false;
    }

    /**
     * Simulates an oil painting
     * @link http://php.net/manual/en/imagick.oilpaintimage.php
     * @param float $radius - The radius of the circular neighborhood.<br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function oilPaint($radius = 3.)
    {
        if (is_numeric($radius)) {
            return $this->imagick->oilPaintImage($radius);
        }

        return false;
    }

    /**
     * Creating a parallelogram
     * @link http://php.net/manual/en/imagick.shearimage.php
     * @param mixed $background - The background color<br/>
     * @param float $shearX - The number of degrees to shear on the x axis<br/>
     * @param float $shearY - The number of degrees to shear on the y axis<br/>
     * @return bool <b>TRUE</b> on success.
     */
    public function shear($background = "#5c7f7f", $shearX = 10., $shearY = 5.)
    {
        if (is_numeric($shearX) && is_numeric($shearY)) {
            return $this->imagick->shearimage($background, $shearX, $shearY);
        }

        return false;
    }

    /**
     * Applies a custom convolution kernel to the image
     * @link http://php.net/manual/en/imagick.convolveimage.php
     * @param array $kernel - The convolution kernel<br/>
     * @param int $channel [optional]  Provide any channel constant that is valid for your channel mode. To apply
     * to more than one channel, combine channeltype constants using
     * bitwise operators. Refer to this list of channel constants.<br/>
     * @return bool <b>TRUE</b> on success.
     */
    //TODO разобраться с матрицей
    public function convolve(array $kernel, $channel = Imagick::CHANNEL_ALL)
    {
        $edgeFindingKernel = array(-1, -1, -1, -1, 8, -1, -1, -1, -1,);
        //TODO - this does nothing.
        $this->imagick->getQuantumRange();
        $this->imagick->setImageBias(3 * floatval($this->imagick->getQuantumRange()));

        return $this->imagick->convolveImage($edgeFindingKernel);
    }
///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////
///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////
///// ///// ///// ///// /////  not tested or not well documented  ///// ///// ///// ///// /////

    /**
     * Annotates an image with text
     * @link http://php.net/manual/en/imagick.annotateimage.php
     * @param string $strokeColor <p>
     * The ImagickDraw object that contains settings for drawing the text<br/>
     * @param string $fillColor <p>
     * @param float $x - Horizontal offset in pixels to the left of text <br/>
     * @param float $y - Vertical offset in pixels to the baseline of text <br/>
     * @param float $angle - The angle at which to write the text$strokeColor
     * @param string $title - The string to draw$strokeColor
     * @return bool <b>TRUE</b> on success.
     */
    // TODO to explain input params
    public function annotate($strokeColor = '#00000000', $fillColor = '#03040305', $x = 40., $y = 40., $angle = 10., $title = "Lorem Ipsum!")
    {
        $draw = new \ImagickDraw();
        $draw->setStrokeColor($strokeColor);
        $draw->setFillColor($fillColor);

        $draw->setStrokeWidth(2);
        $draw->setFontSize(36);

        $draw->setFont('/fonts/Arial.ttf');
//    $this->imagick->setFont('/fonts/Arial.ttf');
        return $this->$imagick->annotateimage($draw, $x, $y, $angle, $title);
    }

    public function equalize()
    {
        return $this->imagick->equalizeImage();
    }

    public function getPixel()
    {
        $imageIterator = $this->imagick->getPixelIterator();

        /** @noinspection PhpUnusedLocalVariableInspection */
        foreach ($imageIterator as $row => $pixels) { /* Loop trough pixel rows */
            foreach ($pixels as $column => $pixel) { /* Loop through the pixels in the row (columns) */
                /** @var $pixel \ImagickPixel */
                if ($column % 2) {
                    $pixel->setColor("rgba(0, 0, 0, 0)"); /* Paint every second pixel black*/
                }
            }
            $imageIterator->syncIterator(); /* Sync the iterator, this is important to do on each iteration */
        }
    }

    public function normalize()
    {
        $original = clone $this->imagick;
        $original->cropimage($original->getImageWidth() / 2, $original->getImageHeight(), 0, 0);
        $this->imagick->normalizeImage(\Imagick::CHANNEL_ALL);
        $this->imagick->compositeimage($original, \Imagick::COMPOSITE_ATOP, 0, 0);
    }

    public function orderedPosterize($orderedPosterizeType = 'o4x4', $channel = Imagick::CHANNEL_ALL)
    {
        $channel = $this->getConstantFromString($channel);
        if (is_string($orderedPosterizeType) && is_numeric($channel)) {
            $this->imagick->orderedPosterizeImage($orderedPosterizeType, $channel);
            return $this->imagick->setImageFormat('png');
        }
        return false;
    }

    public function randomThreshold($lowThreshold = 0.1, $highThreshold = 0.9, $channel = Imagick::CHANNEL_ALL)
    { //0.1 0.9 ALL
        if (is_numeric($lowThreshold) && is_numeric($highThreshold)) {
            return $this->imagick->randomThresholdimage($lowThreshold, $highThreshold, $channel);
        }
        return false;
    }

    public function spread($radius = 1)
    { //1
        if (is_numeric($radius)) {
            return $this->imagick->spreadImage($radius);
        }
        return false;
    }

    public function swirl($swirl = 50)
    {   //100
        if(is_numeric($swirl)){
            return $this->imagick->swirlImage($swirl);
        }
        return false;
    }


///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////


///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////
///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////
///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////
///// ///// ///// ///// /////                                           ///// ///// ///// ///// /////
///// ///// ///// ///// /////        end of basic filters block         ///// ///// ///// ///// /////
///// ///// ///// ///// /////                                           ///// ///// ///// ///// /////
///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// ///// /////

    /**
     * @param $constantName
     * @return mixed
     */
    private function getConstantFromString($constantName){
        if (is_string($constantName) && defined($constantName) == true) {
            return constant($constantName);
        }

        return $constantName;
    }

    /**
     * @param string $filename
     * @return bool
     */
    public function saveImage($filename = 'output')
    {
        return $this->imagick->writeimage($filename . '.' . $this->extension);
    }

}