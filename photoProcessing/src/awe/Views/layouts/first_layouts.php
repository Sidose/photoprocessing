<?php
$tmpDir = './../../temp/';
$newSessionDir = $tmpDir . $_COOKIE['SID'];
?>
<!DOCTYPE html>

<head>
    <title> AWE &#8212; photo image filter factory </title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="en"/>
    <meta name="Author" content="Andy Man & Sergio">
    <meta name="Keywords"
          content="AWE &#8212; photo image filter factory, php, image, image effects, image processing, photo filter, photo effects, online image filter, online photo filter, online photo filter, online photo effect">
    <meta name="description" content="photo image filter">

    <link rel="stylesheet" type="text/css" href="/<?= SITE_ROOT_NAME ?>web/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="/<?= SITE_ROOT_NAME ?>web/css/style.css">
    <script type="text/javascript" src="/<?= SITE_ROOT_NAME ?>web/js/jquery.js"></script>
    <script type="text/javascript" src="/<?= SITE_ROOT_NAME ?>web/js/script.js"></script>

    <link rel="stylesheet" type="text/css" href="/<?= SITE_ROOT_NAME ?>web/css/stylecar.css">
    <link rel="stylesheet" type="text/css" href="/<?= SITE_ROOT_NAME ?>web/css/jcarousel.simple.css">
    <script type="text/javascript" src="/<?= SITE_ROOT_NAME ?>web/js/jquery.jcarousel.min.js"></script>
    <script type="text/javascript" src="/<?= SITE_ROOT_NAME ?>web/js/jcarousel.simple.js"></script>
    <script type="text/javascript" src="/<?= SITE_ROOT_NAME ?>web/js/jquery-ui.min.js"></script>
</head>
<body>


<div class="main_container">

    <header id="header" class="border_frame">
        <nav>
            <div class="container">
                <ul>
                    <li class="yellow"><a href="/<?= SITE_ROOT_NAME ?>index.php"<i>Main</i></a></li>
                    <li class="green"><a href="/<?= SITE_ROOT_NAME ?>category/?id=1"><i>Filters</i></a></li>
                    <li class="blue"><a href="/<?= SITE_ROOT_NAME ?>category/?id=2"><i>Effects</i></a></li>
                    <li class="red"><a href="/<?= SITE_ROOT_NAME ?>category/?id=3"><i>Frames</i></a></li>
                    <li class="purple"><a href="/<?= SITE_ROOT_NAME ?>category/?id=4"><i>Textures</i></a></li>
                    <li class="cyan"><a href="/<?= SITE_ROOT_NAME ?>category/?id=5"><i>Vignettes</i></a></li>
                </ul>
            </div>
            <div class="clear"></div>
        </nav>
    </header>

    <div id="filterform"></div>

    <div id="main" class="border_frame">
        <!---------- control button for upload, undo, forvard, revers and saving ------------>
        <!------------------------------------ START ------------------------------------>
        <div class="app-name" id="header">AWE &#8212; photo image filter factory
            <div id="about"><img src="./../web/img/About.gif" width="15" height="15"></div>
        </div>

        <div id="controls">
            <img src="/<?= SITE_ROOT_NAME ?>web/img/upload.png" id="upload_file" title="Upload Image">
            <img src="/<?= SITE_ROOT_NAME ?>web/img/revert.png" id="original" title="Back To Original">
            <img src="./../web/img/undo.png" id="undo" title="Undo Action">
            <img src="./../web/img/forward.png" id="forward" title="Forward Action">
            <img src="./../web/img/save.png" id="save" title="Save Image">
            <br>
            <!------------------------------------- END ------------------------------------->

            <div id="preview_filters">
                <?php
                include($contentPage);
                ?>
            </div>
        </div>
    </div>

    <?php
    $hide = "hidden";
    if (isset($_SESSION['imgArray']) && null !== $_SESSION['imageIterator']) {
        echo '<input type="' . $hide . '" name="new_uploaded_image" id="new_uploaded_image" value="' . $_SESSION['imgArray'][0] . '">';
        echo '<input type="' . $hide . '" name="active_image" id="active_image" value="' . $_SESSION['imgArray'][$_SESSION['imageIterator']] . '">';
    } else {
        echo '<input type="' . $hide . '" name="new_uploaded_image" id="new_uploaded_image" value="">';
        echo '<input type="' . $hide . '" name="active_image" id="active_image" value="">';
    }
    //        echo '<input type="" name="active_image" id="" value="' .  $_SESSION['imageIterator'] . '">';
    ?>

    <!------------------------------------ Upload box ------------------------------------>
    <div class="boxShadow">
        <div class="dialog draggable" id="fileupload_box">
            <div class="d_header">
                <div id="d_title">Upload file</div>
                <div class="close-button" id="upload-close"><img src="./../web/img/Delete.gif" width="15" height="15">
                </div>
            </div>
            <div id="b_body">
                <form name="upload_form" class="d_upload" action="../../src/awe/core/upload.php" method="POST"
                      target="submit_frame" enctype="multipart/form-data">
                    <input type="file" name="img" placeholder="Select Your Image" accept="image/*"/>
                    <input type="submit" name="upload_image" value="Upload">
                </form>
                <iframe name="submit_frame" src="" id="iframe" frameborder="0">
                    <link href="css/style.css" rel="stylesheet" type="text/css">
                </iframe>
                <div class="center>">
                    <input type="button" id="done" value="Done">
                    <br>
                </div>
            </div>
        </div>
    </div>
    <!------------------------------------- END ------------------------------------->

    <div class="dialog draggable" id="about-box">
        <div id="d_header">
            <br><br><br>

            <div id="d_title">About AWE</div>
            <br>

            <div class="close-button" id="about-close"><img src="./../web/img/Delete.gif" width="15" height="15"></div>
        </div>

        <div id="blue-bold">AWE &#8212; photo image filter factory</div>
        is a PHP tool to get Web image effects on the fly.<br>
        &nbsp;&nbsp;&nbsp;We hope that it will be useful!
        <br>
        <br>

        <div align="center"><input type="button" id="about-ok" value="Ok"></div>
    </div>

    <!----------------------------------- progbar ----------------------------------->
    <div class="border_frame editor_div"
    ">
    <div id="loading">
        <img src="./../web/img/progbar.gif" id="s">
    </div>
    <div id="editor">
        <?php
        if (isset($_SESSION['imgArray']) && null !== $_SESSION['imageIterator']) {
            echo '<img id="777" class="draggable" src="' . $newSessionDir . '/' . $_SESSION['imgArray'][$_SESSION['imageIterator']] . '">';
//                var_dump($_SESSION['imageIterator']);
//                var_dump($_SESSION['imgArray']);
//                var_dump($_COOKIE['SID']);
        }
        ?>
    </div>
</div>

<!-- -------------------------------- ------- --------------------------------- -->
<!-- -------------------------------- ------- --------------------------------- -->
<!-- -------------------------------- ------- --------------------------------- -->

</div>

<footer>
    &copy <?php echo date('Y') ?> all rights reserved. Powered by Manoylov AC & Sydorenko SO
</footer>
</body>
</html>