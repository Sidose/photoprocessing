$(document).ready(function(){
    var save_file_path_php = "../../src/awe/core/save_file.php?file=../";
    var imgPathOriginal = "../../temp/" + getCookie('SID');

    function getCookie(name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    var y_Offset = 25;
    var x_Offset = -10;
//    $("#filterform").fadeTo("fast","0.7");

    // нажатие на кнопку "upload image", после чего появляется форма для выбора загружаемого изображения
    $("#upload_file").click(function(){
        $("#filterform").fadeIn("fast",function(){
//            $("#editor").fadeOut("slow");
            $("#fileupload_box").fadeIn("slow");
        });
    });

    // закрытие формы  загрузки (нажатие на крестик)
    $(".close-button").click(function(){
        $(".dialog").fadeOut("fast",function(){
            $("#filterform").fadeOut("fast");
        });
    });

    // закрытие формы загрузки после согласия на загруженную картинку (нажатие на кнопку done)
    $("#done").click(function(){
        $(".dialog").fadeOut("fast",function(){
            $("#filterform").fadeOut("slow",function(){
                set_image(imgPathOriginal);
            });
        });
    });

    // нажатие на кнопку Upload, что находится в форме загрузки
    $("input[name='upload_image']").click(function(){
        $("#iframe").attr("src","../img/loading.gif");
        set_image(imgPathOriginal);
    });

    $("#about-ok").click(function(){
        $(".close-button").trigger("click");
    });

    // выделение и подписи картинок при наведении
    $("#controls img").hover(function(e){
            this.t = this.title;
            this.title = "";
            $("body").append("<p id='tooltip'>"+ this.t +"</p>");
            $("#tooltip")
                .css("top",(e.pageY + y_Offset) + "px")
                .css("left",(e.pageX + x_Offset) + "px")
                .fadeIn("fast");
        },
        function(){
            this.title = this.t;
            $("#tooltip").remove();
        });

    $("#controls img").mousemove(function(e){
        $("#tooltip")
            .css("top",(e.pageY + y_Offset) + "px")
            .css("left",(e.pageX + x_Offset) + "px");
    });


    ////////////////////////////////////////////////////////////////////////////////////////////////
    // применение фильтров, вперед/назад по историию, сохранение
    //////////////////////////////////////////////////////////////////////////////////////////////// start
    $("#controls img[id!='upload_file']").click(function(){
        var new_uploaded_image = $("#new_uploaded_image").val();
        var active_image = $("#active_image").val();
        var act = $(this).attr("id");
        var extra_params = "";
        if("" != new_uploaded_image && "tmp.jpg" != new_uploaded_image){
            if(act == "save"){
                var filename = $("#editor img").attr("src");
                $("#iframe").attr("src", save_file_path_php + filename);
                return false;
            } else {
                extra_params = "";
            }

            if(active_image == "" && new_uploaded_image ){
                $("#active_image").val(new_uploaded_image);
            }

//            $("#loading").fadeIn("slow");
//            $("#editor").fadeOut("slow",function(){
            $.ajax({
                type: "POST",
                url: "../../src/awe/core/processImage.php",
                data: "act="+act+"&img=" + active_image + extra_params,
                success: function(file_name){
                    if(file_name != "0"){
                        $("#editor").html('<img class="draggable" id="33" src="' + imgPathOriginal + "/" + file_name + '">' ).fadeIn("slow");
//                            +'<br> 777 <br> ' + getCookie('SID') ).fadeIn("slow");
                        $("#active_image").val(file_name);
                    }
                    else{
                        $("#editor").html("<span class='error'>Image not found...!</span>")
                    }
                }
            });
//                $("#loading").fadeOut("slow");
//            });
        }
        else
            alert("Please upload Image");
    });
    //////////////////////////////////////////////////////////////////////////////////////////////// end


// возможность двигать модальное окно с загрузкой изображения
    $(function() {
        $( ".draggable" ).draggable();
    });


    $("#about").click(function(){
        $("#filterform").fadeIn("slow",function(){
            //$("#editor").fadeOut("slow");
            $("#about-box").fadeIn("slow");
        });
    });
});


function set_name(name)
{
    window.parent.document.getElementById("new_uploaded_image").value = name;
    window.parent.document.getElementById("active_image").value = name;
}

function set_image(path)
{
    var editor = $("#editor");
    editor.html('<img name="555" class="draggable" src="' + path + '/' + $("#new_uploaded_image").val() +'">');
    editor.fadeIn("slow");
}