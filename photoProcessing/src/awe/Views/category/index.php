<?php if ($filters): ?>
    <h1 class="hcategory"><?= $category['name']; ?></h1>
    <div id="filters">
        <div class="carousel-wrapper">
            <div data-jcarousel="true" class="carousel"> <!--id="carouselFrame">-->
                <ul>
                    <?php foreach ($filters as $oneFilter): ?>
                        <li class="filter">
                            <img
                                src="../../../../resourses/previews/<?= $oneFilter['preview'] . '.jpg" title="' . $oneFilter['name'] . '" id="' . $oneFilter['id'] ?> ">

                            <p> <?= $oneFilter['name'] ?></p>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <a data-jcarousel-control="true" data-target="-=3" href="#" class="carousel-control-prev">&lsaquo;</a>
            <a data-jcarousel-control="true" data-target="+=3" href="#" class="carousel-control-next">&rsaquo;</a>
        </div>
    </div>
<?php else: ?>
    <h2 class="hcategory">There are no filters in <?= $category['name']; ?> Category</h2>
<?php endif; ?>