<?php

Class Controller_Category Extends Controller_Base
{
    public $layouts = "first_layouts";

    function index()
    {
        $idCategory = (isset($_GET['id'])) ? (int)$_GET['id'] : false;

        if ($idCategory) {
            $select = array(
                'where' => "category_id = $idCategory", // условие
//				'order' => 'date_create DESC' // сортируем
            );
            $model = new Model_Filter($select); // создаем объект модели
            $filters = $model->getAllRows(); // получаем все строки

            $model = new Model_Category();
            $category = $model->getRowById($idCategory);

        } else {
            $filters = false;
            $category = false;
        }

        $this->template->vars('filters', $filters);
        $this->template->vars('category', $category);
        $this->template->view('index');
    }
}