<?php
//session_start();
error_reporting(E_ALL);
include_once('../../../app/config/config.php');

include_once('FilterffectRW.php');
include_once('classes/Model_Base.php');
include_once('../Models/Model_Filter.php');
include_once('../Models/Model_Overlaying_Image.php');

$dbObject = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
$dbObject->exec('SET CHARACTER SET utf8');

$pathToSessionTemp = '../../../temp/' . $_COOKIE['SID'] . '/';

$actionName = $_POST['act'];
$imageName = $_POST['img'];
$picture = $pathToSessionTemp . $imageName;

$imageFF = new FilterFactory($pathToSessionTemp . $imageName);
$filef = new FilterffectRW();

if ($actionName != "undo" && $actionName != "forward") {
    if (file_exists($picture)) {
        if ($actionName == "original") {
            $_SESSION['imageIterator'] = 0;
            echo $_SESSION['imgArray'][0];
            exit;
        }

        ///////////////////// запрос в бд ///////////////////////////////////

        $tempFilterSelect = $actionName; // id
        $select = array(
            'where' => "id = {$tempFilterSelect} " // условие
        );

        $model = new Model_Filter($select); // создаем объект модели
        $filters = $model->getOneRow(); // получаем фильтр

        $params = [];

        /// если это эффект, то у него будет хотя бы один 'idf=' и мы выполняем код по вытаскиванию фильтров
        if (strpos($filters['settings'], 'idf=') !== false) {

            $pattern = '/:\[[\d\.,"]*\]}/'; // паттерн для поиска значений фильтров. Выраженя ":[4]"
            preg_match_all($pattern, $filters['settings'], $params); // находим только массивы параметров
            $params = preg_replace('/[:}]*/', '', $params[0]); // убираем лишние символы ":}" - остается массив

            // преобразуем $params пошучаем двухмерный массив параметрами
            for ($i = 0; $i < count($params); ++$i) {
                $params[$i] = json_decode($params[$i]);
            }

            $pattern = '/"idf=\d+"/'; // паттерн для поиска выраженя "idf=???"
            preg_match_all($pattern, $filters['settings'], $matches); // сбор совпадений с паттерном
            $pattern = '/\d+/';
            preg_match_all($pattern, json_encode($matches), $matches); // сбор только номеров айдишников

            $tempFilterSelect = '';
            //создание запросов только для нунжных айди
            for ($i = 0; $i < count($matches[0]); ++$i) {
                if ($i !== 0) {
                    $tempFilterSelect .= ' OR ';
                }
                $tempFilterSelect .= "id = {$matches[0][$i]}";
            }

//////////////////

            $select = array(
                'where' => " {$tempFilterSelect} " // условие
            );

            $model = new Model_Filter($select); // создаем объект модели с запросом нужных фильтров
            $filters = $model->getAllRows(); // получаем фильтр/фильтры

            $effect['settings'] = '';

            for ($i = 0; $i < count($filters); $i++) {
                $effect['settings'] .= $filters[$i]['settings'];
            }

            $effect['settings'] = str_replace('][', ',', $effect['settings']);

            if (isset($effect['settings'])) {
                $effectParamsBuf = json_decode($effect['settings']);

                for ($i = 0; $i < count($effectParamsBuf); ++$i) {
                    foreach ($effectParamsBuf[$i] as &$filterArray) {
                        for ($j = 0; $j < count($filterArray); ++$j) {
                            if (isset($params[$i][$j])) {
                                $filterArray[$j] = $params[$i][$j];
                            }
                        }
                    }
                }

                unset($filterArray);
                unset($param);

                $filters['settings'] = json_encode($effectParamsBuf);
            }
        }

        if (isset($filters['settings']) && strpos($filters['settings'], 'idp=') !== false) {

            $pattern = '/"idp=\d+"/'; // паттерн для поиска выраженя "idp=???"
            preg_match_all($pattern, $filters['settings'], $matches); // сбор совпадений с паттерном
            $pattern = '/\d+/';
            preg_match_all($pattern, json_encode($matches), $matches); // сбор только номеров айдишников

            $tempSelect = '';
            //создание запросов только для нужных айди
            for ($i = 0; $i < count($matches[0]); ++$i) {
                if ($i !== 0) {
                    $tempSelect .= ' OR ';
                }
                $tempSelect .= "id = {$matches[0][$i]}";
            }

            $adittionsSourse = '';
            //вытаскивание из БД нужных полей (дополнитеьлные ресурсы изображений)
            if ('' != $tempSelect) {
                $selectForOverlayingImage = ['where' => $tempSelect];
                $modelOverlayingImage = new Model_Overlaying_Image($selectForOverlayingImage);
                $adittionsSourse = $modelOverlayingImage->getAllRows();

                //замена айдишников дополнительных ресурсов на прямой путь
                for ($i = 0; $i < count($adittionsSourse); ++$i) {
                    $search = '"idp=' . $adittionsSourse[$i]['id'] . '"';
                    $replace = json_encode(realpath(BASE_RESOURSE_PATH . $adittionsSourse[$i]['path']));
                    $filters['settings'] = str_replace($search, $replace, $filters['settings']);
                }
            }
        }
////////////////////////////////////////////////////////

        $filef->applyFilterListEffect($imageFF, $filters['settings'], false);
        $image_new_name = time();
        $_SESSION['imgArray'][++$_SESSION['imageIterator']] = $image_new_name . '.' . $imageFF->getExtension();
        savePicture($imageFF, $image_new_name);

        if ($_SESSION['imageIterator'] + 1 != count($_SESSION['imgArray'])) {
            array_splice($_SESSION['imgArray'], $_SESSION['imageIterator'] + 1);
        }

    } else
        echo "0"; // Image not found...!
} elseif ($actionName == "undo") {
    //TODO If prev index = 0 then hide undo button

    if ($_SESSION['imageIterator'] - 1 > 0) {
        echo $_SESSION['imgArray'][--$_SESSION['imageIterator']];
    } else {
        $_SESSION['imageIterator'] = 0;
        echo $_SESSION['imgArray'][$_SESSION['imageIterator']];
    }
} elseif ($actionName == "forward") {
    if ($_SESSION['imageIterator'] + 1 < count($_SESSION['imgArray'])) {
        echo $_SESSION['imgArray'][++$_SESSION['imageIterator']];
    } else {
        echo $_SESSION['imgArray'][$_SESSION['imageIterator']];
    }
}

function savePicture(FilterFactory $imageFF, $filename)
{
    $pathToSessionTemp = '../../../temp/' . $_COOKIE['SID'] . '/';
    $imageFF->saveImage(realpath($pathToSessionTemp) . '/' . $filename);

    echo $filename . '.' . $imageFF->getExtension(); //print image name
}