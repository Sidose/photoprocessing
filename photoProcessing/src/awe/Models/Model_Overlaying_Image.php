<?php

Class Model_Overlaying_Image Extends Model_Base
{

    public $id;
    public $name;
    public $path;
    public $description;

    public function fieldsTable()
    {
        return array(
            'id' => 'id',
            'name' => 'name',
            'path' => 'path',
            'description' => 'description',
        );
    }
}