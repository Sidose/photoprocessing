<?php
include_once('FilterFactory.php');

class FilterffectRW
{
    private $effectsSettingsArray = [];

    public function __construct()
    {

    }

    public function applyOneFilter(FilterFactory $classObj, $nameFilter, $filterParams, $isLog = true)
    {
        if (isset($classObj) && method_exists($classObj, $nameFilter)) {
            call_user_func_array([$classObj, $nameFilter], $filterParams);
            if ($isLog) {
                $this->effectsSettingsArray[] = [$nameFilter => $filterParams];
            }
        }
    }

    /**
     * @return array
     */
    public function getEffectsSettingsArray()
    {
        return $this->effectsSettingsArray;
    }


    public function getJsonSettings()
    {

        return json_encode($this->effectsSettingsArray);
    }

    /**
     * method that write to file all filters with the parameters applied to the image
     */
    public function writeJsonToFIle()
    {
        $file = fopen('test.txt', 'w');
        $tmp = str_replace(trim(json_encode(BASE_RESOURSE_PATH), '"'), '', json_encode($this->effectsSettingsArray));
        fputs($file, $tmp);
        fclose($file);
    }

    public function makePreview(FilterFactory $imageToPreview, $outputName, $width = 100, $height = 100)
    {
        $this->applyOneFilter($imageToPreview, "cropThumbnail", [$width, $height], false);
        $this->applyFilterListEffect($imageToPreview, $this->getJsonSettings(), false);
        $this->applyOneFilter($imageToPreview, "saveImage", [$outputName], false);
    }

    /**
     * @param $classObj
     * @param $filterList
     * @param bool $isLog
     * @return bool
     */
    public function applyFilterListEffect($classObj, $filterList, $isLog = true)
    {
        $filterList = json_decode($filterList, true);

        $isAllGood = true;
        if (is_array($filterList)) {
            foreach ($filterList as $oneFilter) {
                if (isset($filterList) && is_array($filterList)) {
                    foreach ($oneFilter as $filterName => $filterParams) {
                        $this->applyOneFilter($classObj, $filterName, $filterParams, $isLog);
                    }
                }
            }
        }

        return boolval($isAllGood);
    }

}