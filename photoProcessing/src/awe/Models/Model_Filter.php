<?php

Class Model_Filter Extends Model_Base
{
    public $id;
    public $name;
    public $preview;
    public $settings;
    public $popularity;
    public $description;
    public $category;

    public function fieldsTable()
    {
        return array(

            'id' => 'id',
            'name' => 'name',
            'preview' => 'preview',
            'settings' => 'settings',
            'popularity' => 'popularity',
            'description' => 'description',
            'category' => 'category',
        );
    }

}