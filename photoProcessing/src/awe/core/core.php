<?php
function __autoload($className)
{
    $filename = strtolower($className) . '.php';

    // определяем класс и находим для него путь
    $expArr = explode('_', $className);

    if (empty($expArr[1]) OR $expArr[1] == 'Base') {
        $folder = 'src' . DS . 'awe' . DS . 'core' . DS . 'classes';
    } else {
        switch (strtolower($expArr[0])) {
            case 'controller':
                $folder = 'src' . DS . 'awe' . DS . 'Controllers';
                break;

            case 'model':
                $folder = 'src' . DS . 'awe' . DS . 'Models';
                break;

            default:
                $folder = 'src' . DS . 'awe' . DS . 'core' . DS . 'classes';
                break;
        }
    }

    $filePath = $folder . DS . $filename;

    // проверяем наличие файла
    if (file_exists($filePath) == false) {
        return false;
    }
    // подключаем файл с классом
    include($filePath);
}